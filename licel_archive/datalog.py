import datetime
import codecs


class DataLog(object):
    """ A class representing a Raymetrics data-log file.

    These files are a database of measurements performed by the user. """

    def __init__(self, file_path):
        """
        A data-log object.

        Parameters
        ----------
        file_path : str
           Path to datalog file.
        """
        self.file_path = file_path

        self.datasets = []
        self.raw_entries = self._read_entries()
        self.records = self._get_records()

    def _read_entries(self):
        """ Read the datalog file. """
        with open(self.file_path, 'r') as f:
            data = f.read()

        total_length = len(data)
        if total_length==0:
            raise IOError("Data log appears to be empty.")

        entries = []

        position = 0
        while position < total_length:
            length_str = data[position:position + 4]
            length = int(codecs.encode(length_str, 'hex'), 16)
            text = data[position + 4:position + 4 + length]
            entries.append(text)
            position += 4 + length

        return entries

    def _get_records(self):
        """ Convert the list of raw entries to measurement records. """
        # Split the list in groups of 4
        groups = [self.raw_entries[i:i + 4] for i in range(0, len(self.raw_entries), 4)]

        records = []
        for group in groups:
            record = {'user_name': group[0],
                      'location': group[1],
                      'start_date': self._str_to_date(group[2]),
                      'stop_date': self._str_to_date(group[3])}
            records.append(record)
        return records

    def print_records(self, min_idx=0, max_idx=None, date_fmt='%Y-%m-%d %H:%M:%S'):
        """ Print a list of records. """

        # Get max length of records
        user_length = max([len(record['user_name']) for record in self.records[min_idx:max_idx]])
        location_length = max([len(record['location']) for record in self.records[min_idx:max_idx]])

        print("No  {0:<{user_length}} {1:<{location_length}} Start               Stop".format('User', 'Location', user_length=user_length,
                                                                  location_length=location_length))
        ns = range(len(self.records))
        for n, record in zip( ns[min_idx:max_idx], self.records[min_idx:max_idx]):
            user = record['user_name']
            location = record['location']
            start = record['start_date'].strftime(date_fmt)
            stop = record['stop_date'].strftime(date_fmt)
            print("{0:<3} {1:<{user_length}} {2:<{location_length}} {3} {4}".format(n, user, location, start, stop,
                                                                                   user_length=user_length,
                                                                                   location_length=max(location_length, 8)
                                                                                   ))

    @staticmethod
    def _str_to_date(date_str):
        """ Convert a date string to a datetime object.

        Parameters
        ----------
        date_str : str
           A datetime string in the appropriate format e.g., 00:54:48:34 21/11/2016.

        Returns
        -------
        : datetime object
           The corresponding datetime object.
        """
        return datetime.datetime.strptime(date_str, "%H:%M:%S:%f %d/%m/%Y")
